import java.util.Arrays;
import java.util.Random;
import java.util.Scanner;

class HangMan{
    private static String[] words = { "Selenium", "Iterator", "Instantiate", "Interface", "Abstract", "Instance", "Index", "Variable", "Array", "Asterisk", "Initialize" };

    private static int maxIncorrectGuesses = 5;  // Maximum number of times we can guess incorrectly before the game ends
    private static int incorrectGuessCounter = 0;  // Number of times we've already guessed incorrectly

    private static String hiddenWord; // We'll chose a random word from words, and save it here. This is the word the user will try to guess
    private static char[] shownWord; // Array of * characters representing each letter from hiddenWord. Each time a letter is guessed correctly the matching * is replaced with the letter
    private static char[] incorrectGuessedCharacters = new char[ maxIncorrectGuesses ];

    public static void main(String[] args){
        startGame();
        while ( gameIsRunning() ){  // Keep looping until we guess the word correctly
            char guess = promptCharGuess();  // Ask the user to guess a character
            boolean correctGuess = false;  // Use this flag to signal that the user made a correct guess

            // Check if any letters of hiddenWord match the guess
            for( int i = 0; i < hiddenWord.length(); i++ ){
                char hiddenWordChar = hiddenWord.toCharArray()[i];

                // make both the characters lowercase before checking if they're equal, that way 'a' will match both the 'A' and 'a' in "Array" for { 'A', '*', '*', 'a', '*' } shownWord
                if( Character.toLowerCase( guess ) == Character.toLowerCase( hiddenWordChar ) ){
                    correctGuess = true;
                    shownWord[ i ] = hiddenWordChar;
                }
            }

            if( !correctGuess ){  // Let the user know it was a bad guess
                System.out.println( "Bad guess, "+ ( maxIncorrectGuesses - ( incorrectGuessCounter + 1 ) ) + " guesses remain" );
                incorrectGuessedCharacters[ incorrectGuessCounter ] = guess;  // Store this guess so we can't guess it again
                incorrectGuessCounter ++;  // Increment the bad guess counter so that we're one step closer to GAME OVER
            }
        }
    }

    private static void startGame(){
        /* Pick a random word, set up hiddenWord and shownWord */
        hiddenWord = getNextWord();
        shownWord = new char[ hiddenWord.length() ];
        // Set every char of shownWord to *
        for( int i = 0; i < hiddenWord.length(); i++ ){
            shownWord[i] = '*';
        }

    }

    private static String getNextWord(){
        /* Picks a random word from the words array */
        return "TODO";
    }

    private static boolean gameIsRunning(){
        // the game is running when incorrectGuessCounter is less than maxIncorrectGuesses and shownWord is not equal to hiddenWord
        return true;
    }

    private static char promptCharGuess(){
        // Ask the user to input a character, check if the input String is valid using validCharGuess
        String guess = "Test"; // Use a scanner to input a guess
        if( validCharGuess( guess )){
            return guess.charAt(0);
        } else {
            return promptCharGuess();  // If it wasn't valid, start from the top again, this is called Recursion. It's an alternative to a while loop
        }

    }

    private static boolean validCharGuess( String guess ){
        /* Return true if the guess is a single character, and the guess has not been made before */
        return false;
    }
}